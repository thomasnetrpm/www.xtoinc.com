   <?php if( is_page( array( 1254, 1252, 1253, 1251 ) ) ):  ?><!-- CTA -->
      <section class="full-width-cta" style="background-image: url(<?php echo get_field('fwc_image','option'); ?>);">
          <div class="inner-wrap">
            <h3 class="fwc-heading"><?php if(get_field('fwc_text','option')): echo get_field('fwc_text','option'); endif; ?></h3>
              <?php 
                            $fwc_link = get_field('fwc_link','option');
                            if( $fwc_link ): 
                                $link_url = $fwc_link['url'];
                                $link_title = $fwc_link['title'];
                            ?>  <a href="<?php echo esc_url($link_url); ?>" class="btn fwc-btn"><?php echo esc_html($link_title); ?></a><?php endif; ?>
          </div>
        </section>	
      <!-- CTA--><?php  endif; ?>
  <!--Site Footer Starts-->
    <footer class="site-footer" role="contentinfo">
      <div class="inner-wrap">
        
      <div class="sf-footer">
        <div class="sf-logo-wrap"><a href="<?php bloginfo('url'); ?>"><?php $logo = get_field("global_company_footer_logo","option");
              if (!empty($logo)): ?><img src="<?php echo $logo['url']; ?>" class="sf-logo" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['alt']; ?>"><?php endif; ?></a></div>
        <div class="sf-links-one">
        	 <?php wp_nav_menu(array(
              'menu'            => 'Other Menu',
              'container'       => 'ul',
              'menu_class'      => 'sf-link-list',
              )); ?>
         
        </div>
        <div class="sf-links-two">
        	 <?php wp_nav_menu(array(
              'menu'            => 'Footer Nav',
              'container'       => 'ul',
              'menu_class'      => 'sf-link-list',
              )); ?>
          
        </div>
        <div class="sf-cont-det">
          <p class="sf-toll">Toll Free: <?php $string = get_field('global_phone_number','option');$string = preg_replace("/[^0-9]/", '', $string);?><a href="tel:<?php echo $string;?>"><?php echo get_field('global_phone_number','option');?></a></p>
          <p class="sf-ph">Ph: <?php $string2 = get_field('global_phone_number_no-format','option');$string2 = preg_replace("/[^0-9]/", '', $string2);?><a href="tel:<?php echo $string2;?>"><?php echo get_field('global_phone_number_no-format','option');?></a></p>
          <p class="sf-fax">Fax: <a href="javascript:void(0);" tabindex="-1"><?php if (get_field('global_fax','option')):echo get_field('global_fax','option'); endif; ?></a></p>
         <?php 
                            $global_rfq_link = get_field('global_rfq_link','option');
                            if( $global_rfq_link ): 
                                $link_url = $global_rfq_link['url'];
                                $link_title = $global_rfq_link['title'];
                            ?> <a href="<?php echo esc_url($link_url);?>" class="btn sf-btn"><?php echo esc_html($link_title);?></a><?php endif; ?>
        </div>
      </div>

      </div>
      <div class="sm-footer">
        <div class="inner-wrap">
          <div class="sf-copyright">
          © <?php echo date("Y"); ?> <a href="<?php bloginfo('url'); ?>"><?php bloginfo( 'name' ); ?></a> <span>|</span>  <?php if (get_field('global_address','option')):echo get_field('global_address','option'); endif; ?> 
        </div>
        <div class="sf-created">
            Site Created by <a href="https://business.thomasnet.com/marketing-services" target="_blank" rel="noopener noreferrer">Thomas Marketing Services</a>
        </div>
        </div>
      </div>
    </footer>
    <!--Site Footer Ends-->