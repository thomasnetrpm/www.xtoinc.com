  <!--Site Intro Starts-->
      <section class="site-intro">
      	<style type="text/css">
 .site-intro:before {
   background-image: url(<?php if(get_field('si_image')) { echo get_field('si_image'); } else { bloginfo('template_url'); ?>/img/hero.jpg <?php }  ?>);
 }
</style>
        <div class="inner-wrap">
          <h1 class="si-header"><?php if(get_field('si_heading')): echo get_field('si_heading'); endif; ?></h1>
          <p class="si-sub-heading"><?php if(get_field('si_sub_heading')): echo get_field('si_sub_heading'); endif; ?></p>
              <?php $si_our_material = get_field('si_our_material');
    if( $si_our_material ): 
      $link_url = $si_our_material['url'];
      $link_title = $si_our_material['title'];
      $link_target = $si_our_material['target'] ? $si_our_material['target'] : '_self';
      ?> <a href="<?php echo esc_url($link_url);?>" class="btn si-mat-btn"><?php echo esc_html($link_title);?></a><?php endif; ?>
         <?php $rfq_btn = get_field('rfq_btn');
    if( $rfq_btn ): 
      $link_url = $rfq_btn['url'];
      $link_title = $rfq_btn['title'];
      $link_target = $rfq_btn['target'] ? $rfq_btn['target'] : '_self';
      ?> <a href="<?php echo esc_url($link_url);?>" class="btn si-rq-btn"><?php echo esc_html($link_title);?></a><?php endif; ?>
        </div>
      </section>
      <!--Site Intro Ends-->