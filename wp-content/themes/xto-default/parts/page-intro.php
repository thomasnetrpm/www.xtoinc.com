  <!--Page Intro Starts-->
      <section class="page-intro site-cont-wrap">
     <style type="text/css">
 .page-intro:before {
   background-image: url(<?php if(get_field('pi_image')) { echo get_field('pi_image'); } else { bloginfo('template_url'); ?>/img/hero.jpg <?php }  ?>);
 }
</style>
          <div class="inner-wrap">
            <?php if(is_home()):?>
  <h1 class="pi-heading">Latest Posts</h1>
 <?php elseif(is_404()):?>
  <h1 class="pi-heading">404: Page not found</h1>
 <?php elseif(is_search()):?>
  <h1 class="pi-heading">Search Results for '<?php echo get_search_query(); ?>'</h1>
 <?php elseif(get_field('pi_heading')):?>
  <h1 class="pi-heading"><?php echo get_field('pi_heading');?></h1>
<?php else: ?>
 <h1 class="pi-heading"><?php the_title(); ?></h1>
<?php endif;?>
          </div>
      </section>
      <!--Page Intro Ends-->
