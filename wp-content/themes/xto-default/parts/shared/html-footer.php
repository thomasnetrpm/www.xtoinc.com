
		<?php wp_footer(); ?>
		<p id="back-top">
		<a href="#top"><span>Back to Top</span></a>
	</p>
	</div>
	<!-- Site Wrap End -->
		<?php if(get_field('before_the_body')):?>
		<?php echo get_field('before_the_body'); ?>
	<?php elseif(get_field('before_the_body','options')):?>
		<?php echo get_field('before_the_body','options'); ?>
	<?php endif;?>
	<?php if(get_field('add_script_to_footer')):?>
            <?php echo get_field('add_script_to_footer'); ?>
        <?php endif;?>
    </body>
</html>