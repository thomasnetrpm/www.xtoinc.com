
<?php if( have_rows('flexible_content') ): echo '<section class="additional-content">';
    while ( have_rows('flexible_content') ) : the_row(); ?>

	<?php if( get_row_layout() == 'tab_content' ): ?>
		<?php if( get_sub_field('fullwidth') == false): ?>
			<section class="accordian-tabs-module">
			 	<div class="inner-wrap">		 	
			 		<?php if( get_sub_field('section_header')): ?>
						<h2><?php echo get_sub_field('section_header'); ?></h2>
					<?php endif; ?>
					<?php if( get_sub_field('section_subtext')): ?>
						<p class="column-subtext"><?php echo get_sub_field('section_subtext'); ?></p>
					<?php endif; ?>

					<ul class="accordion-tabs">
						<?php if( have_rows('tab_content_row') ): while ( have_rows('tab_content_row') ) : the_row(); ?>
							<li class="tab-header-and-content">
								<a href="javascript:void(0)" class="tab-link"><?php echo get_sub_field('tab_header'); ?></a>
								<div class="tab-content"><?php echo get_sub_field('tab_body'); ?></div>							
							</li>
						<?php endwhile; ?>
						<?php endif; ?>
					</ul>
					<?php if( get_sub_field('divider')): ?>
						<hr>
					<?php endif; ?>			
				</div>
			</section>
		<?php endif; ?>

	<?php elseif( get_row_layout() == 'full_width_cta' ): ?>

		<section class="full-width-cta-test">
		<div class="inner-wrap"><h2 class="fwc-sec-heading"><?php if(get_sub_field('section_header')): echo get_sub_field('section_header'); endif;?></h2></div>
          <section class="full-width-cta" style="background-image: url(<?php echo get_sub_field('fwc_image'); ?>);">
          <div class="inner-wrap">
            <h3 class="fwc-heading"><?php if(get_sub_field('fwc_text')): echo get_sub_field('fwc_text'); endif;?></h3>
              <?php $fwc_link = get_sub_field('fwc_link');
    if( $fwc_link ): 
      $link_url = $fwc_link['url'];
      $link_title = $fwc_link['title'];
      $link_target = $fwc_link['target'] ? $fwc_link['target'] : '_self';
      ?>  <a href="<?php echo esc_url($link_url); ?>" class="btn fwc-btn"><?php echo esc_html($link_title); ?></a><?php endif; ?>
          </div>
        </section>		
			<?php if( get_sub_field('divider')): ?>
				<div class="inner-wrap"><hr></div>
			<?php endif; ?>
		</section>		
        

 	<?php elseif( get_row_layout() == 'multiple_columns' ): ?>
 		<section class="multiple-cols-module">
		 	<div class="inner-wrap">	
		 		<?php if( get_sub_field('section_header')): ?>
					<h2><?php echo get_sub_field('section_header'); ?></h2>
				<?php endif; ?>
				<?php if( get_sub_field('section_subtext')): ?>
					<p class="column-subtext"><?php echo get_sub_field('section_subtext'); ?></p>
				<?php endif; ?>
				<section class="<?php if (get_sub_field('number_columns') == '2') {
						echo 'rows-of-2';
					} else if (get_sub_field('number_columns') == '3') {
					        echo 'rows-of-3';
					} else if (get_sub_field('number_columns') == '4') {
					        echo 'rows-of-4';
					}
					?>">

		         	<?php if( have_rows('content') ): while ( have_rows('content') ) : the_row(); ?>
						<div><?php echo get_sub_field('content_column'); ?></div>
					<?php endwhile; ?>
					<?php endif; ?>				
				</section>
				<?php if( get_sub_field('divider')): ?>
					<hr>
				<?php endif; ?>
			</div>
 		</section>	

	<?php elseif( get_row_layout() == 'img_gallery_section' ): ?>
		<?php if( get_sub_field('fullwidth') == false): ?>
			<section class="image-gallery-module">
				<div class="inner-wrap">	
					<?php if( get_sub_field('section_header')): ?>
						<h2><?php echo get_sub_field('section_header'); ?></h2>
					<?php endif; ?>
					<section class="<?php if (get_sub_field('number_columns') == '2') {
								echo 'rows-of-2';
							} else if (get_sub_field('number_columns') == '3') {
							        echo 'rows-of-3';
							} else if (get_sub_field('number_columns') == '4') {
							        echo 'rows-of-4';
							}
							?>">
						<?php $images = get_sub_field('img_gallery');
							if( $images ): ?>
								<?php foreach( $images as $image ): ?>
			                    	<a href="<?php echo $image['sizes']['large']; ?>" class="lightbox loop-item">
				                    	<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>"/>
			                    		<h4 class="li-title"><?php echo $image['caption']; ?></h4>
			                    	</a>
								<?php endforeach; ?>
							<?php endif; ?>
					</section>
					<?php if( get_sub_field('divider')): ?>
							<hr>
					<?php endif; ?>
				</div>
			</section>
		<?php endif; ?>

	<?php elseif( get_row_layout() == 'click_expand' ): ?>
		<?php if( get_sub_field('fullwidth') == false): ?>
			<section class="click-expand-module">
				<div class="inner-wrap">
					<div class="click-expand <?php if( get_sub_field('spacing')): ?>spacing-bottom<?php endif; ?>">
			          <h3 class="ce-header" tabindex="0"><?php echo get_sub_field('section_header'); ?></h3>
			          <div class="ce-body"><?php echo get_sub_field('section_body'); ?></div>
			      	</div>
			    </div>
			</section>	        
		<?php endif; ?>

 			
	<?php elseif( get_row_layout() == 'table' ): ?>
		<section class="tabular-data">
		   <div class="inner-wrap">
		        <?php if( get_sub_field('section_header')): ?>
		            <div class="headexpand-wrap">  
		            	<h2 class="headexpand"><?php echo get_sub_field('section_header'); ?></h2>
				<?php endif; ?>
							<?php if( get_sub_field('section_header')): ?>
								<h3 class="column-subtext"><?php echo get_sub_field('section_subtext'); ?></h3>
							<?php endif; ?>
					        <?php if( get_sub_field('table_content')): ?>
					            <div class="table-wrap">
					                <table class="tablesaw tablesaw-stack" data-tablesaw-mode="stack">
					                	<?php echo get_sub_field('table_content'); ?>
					                </table>
					            </div>
					        <?php endif; ?>
					        <?php if( get_sub_field('section_header')): ?>
		           </div> 
		           <!--headexpand-wrap END -->
		        <?php endif; ?>

		        <?php if( get_sub_field('divider')): ?>
					<hr>
				<?php endif; ?>
			</div>
		</section>	


	<?php elseif( get_row_layout() == 'product_grid' ): ?>
		<section class="product-grid-module">
			<div class="inner-wrap">
				 <h2><?php if(get_sub_field('section_header')): echo get_sub_field('section_header'); endif;?></h2> 
          <section class="bucket">
 <?php if( have_rows('product_row') ):while ( have_rows('product_row') ) : the_row(); ?>
            <?php $pr_link = get_sub_field('pr_link');
    if( $pr_link ): 
      $link_url = $pr_link['url'];
      $link_title = $pr_link['title'];
      $link_target = $pr_link['target'] ? $pr_link['target'] : '_self';
      ?>  <a href="<?php echo esc_url($link_url); ?>" class="pgm-item">
              <figure class="pgm-item-img"><span> <?php $pr_image = get_sub_field('pr_image');
                   if( !empty($pr_image) ): ?><img src="<?php echo $pr_image['url']; ?>" alt="<?php echo $pr_image['alt']; ?>" title="<?php echo $pr_image['alt']; ?>"><?php endif; ?></span></figure>
              <span class="pgm-link"><?php echo esc_html($link_title); ?></span>
            </a><?php endif; ?>
             <?php   endwhile; endif; ?>
          </section>
				<?php if( get_sub_field('divider')): ?>
					<hr>
				<?php endif; ?>
			</div>
		</section>

	<?php elseif( get_row_layout() == 'text_media' ): ?>
		<section class="text-media-module">
			<div class="inner-wrap">
				<?php if( get_sub_field('section_subtext')): ?>
					<p class="column-subtext"><?php echo get_sub_field('section_subtext'); ?></p>
				<?php endif; ?>			
		     	<article class="clearfix">	    		
		    		<div class="col-3of9">
		    			<?php echo get_sub_field('media'); ?>
		    		</div>
		    		<div class="col-6of9 col-last">
			    		<?php if( get_sub_field('section_header')): ?>
						<h2><?php echo get_sub_field('section_header'); ?></h2>
						<?php endif; ?>
		    			<?php echo get_sub_field('text'); ?>
		    		</div>	    		
				</article>
				<?php if( get_sub_field('divider')): ?>
					<hr>
				<?php endif; ?>
			</div>
		</section>

<?php elseif( get_row_layout() == 'service_module' ): ?>
 <!-- Service Module Starts -->
      <section class="services-module">
      		 <?php if( have_rows('sm_items') ): while ( have_rows('sm_items') ) : the_row(); ?>	
       <?php $smi_link = get_sub_field('smi_link');
    if( $smi_link ): 
      $link_url = $smi_link['url'];
      $link_title = $smi_link['title'];
      $link_target = $smi_link['target'] ? $smi_link['target'] : '_self';
      ?> <a href="<?php echo esc_url($link_url); ?>" class="sm-item">
          <span class="sm-image" style="background-image: url(<?php echo get_sub_field('smi_image'); ?>);"></span>
          <span class="sm-text"><?php if(get_sub_field('smi_text')): echo get_sub_field('smi_text'); endif;?></span>
          <span class="sm-btn"><?php echo esc_html($link_title); ?></span>
        </a><?php endif; ?>
        <?php $i++; endwhile; endif; ?> 
      </section>
      <!-- Service Module Ends -->
<?php elseif( get_row_layout() == 'material_module' ): ?>
      <!-- Material Module Starts -->
      <section class="material-module">
        <h2 class="mm-heading"><?php if(get_sub_field('mm_heading')): echo get_sub_field('mm_heading'); endif;?></h2>
        <div class="mm-tabs">
        <ul class="accordion-tabs desti">
	 <?php if( have_rows('mm_items') ):$i=1; while ( have_rows('mm_items') ) : the_row(); ?>	
          <li class="tab-header-and-content"><a href="javascript:void(0)" class="<?php if($i == 1){ echo 'is-active'; } ?> tab-link"><?php if(get_sub_field('mmi_title')): echo get_sub_field('mmi_title'); endif;?></a><div class="tab-content">
              <div class="mm-cont-wrap">
                <div class="mm-img" style="background-image: url(<?php echo get_sub_field('mmi_image'); ?>);"></div>
                <div class="mm-cont">
                  <h3 class="mm-title"><?php if(get_sub_field('mmi_title')): echo get_sub_field('mmi_title'); endif;?></h3>
                  <p class="mm-desc"><?php if(get_sub_field('mmi_desc')): echo get_sub_field('mmi_desc'); endif;?></p>
                 <?php $mmi_link = get_sub_field('mmi_link');
    if( $mmi_link ): 
      $link_url = $mmi_link['url'];
      $link_title = $mmi_link['title'];
      $link_target = $mmi_link['target'] ? $mmi_link['target'] : '_self';
      ?>  <a href="<?php echo esc_url($link_url); ?>" class="btn mm-btn"><?php echo esc_html($link_title); ?></a><?php endif; ?>
                </div>
              </div>
            </div></li>
             <?php $i++; endwhile; endif; ?> 
        </ul>
        </div>
      </section>
      <!-- Material Module Ends -->
<?php elseif( get_row_layout() == 'product_module' ): ?>
      <!-- Product Module Starts -->
      <section class="product-module">
        <h2 class="pm-heading"><?php if(get_sub_field('pm_heading')): echo get_sub_field('pm_heading'); endif;?></h2>
        <div class="pm-item-wrap">
            <?php if( have_rows('pm_items') ):while ( have_rows('pm_items') ) : the_row(); ?>	
        <div class="pm-wrapper">
          <div class="pm-img-wrap">
           <?php $image = get_sub_field('pmi_image');
                   if( !empty($image) ): ?> <img src="<?php echo $image['url'];?>" class="pm-img" alt="<?php echo $image['alt'];?>" title="<?php echo $image['alt'];?>"><?php endif; ?>
          </div>
          <div class="pm-cont-wrap">
            <h3 class="pm-title"><?php if(get_sub_field('pmi_title')): echo get_sub_field('pmi_title'); endif;?></h3>
            <p class="pm-desc"><?php if(get_sub_field('pmi_description')): echo get_sub_field('pmi_description'); endif;?></p>
            <ul class="pm-list">
            	  <?php if( have_rows('pmi_list_items') ):while ( have_rows('pmi_list_items') ) : the_row(); ?>	
              <li><?php $pmi_list_link = get_sub_field('pmi_list_link');
    if( $pmi_list_link ): 
      $link_url = $pmi_list_link['url'];
      $link_title = $pmi_list_link['title'];
      $link_target = $pmi_list_link['target'] ? $pmi_list_link['target'] : '_self';
      ?><a href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a><?php endif; ?></li>
              <?php   endwhile; endif; ?> 
            </ul>
            <?php $pmi_link = get_sub_field('pmi_link');
    if( $pmi_link ): 
      $link_url = $pmi_link['url'];
      $link_title = $pmi_link['title'];
      $link_target = $pmi_link['target'] ? $pmi_link['target'] : '_self';
      ?><a class="btn pm-btn" href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a><?php endif; ?>
          </div> 
        </div>
  <?php   endwhile; endif; ?> 
       

        </div>
      </section>
      <!-- Product Module Ends -->




      
<?php elseif( get_row_layout() == 'our_blogs_module' ): ?>	
      <!-- Our Blogs Module Starts -->
      <section class="our-blog-module">
        <div class="inner-wrap">
          <h2 class="obm-heading"><?php if(get_sub_field('ob_heading')): echo get_sub_field('ob_heading'); endif;?></h2>
          <p class="obm-desc"><?php if(get_sub_field('ob_description')): echo get_sub_field('ob_description'); endif;?></p>
          <div class="obm-wrap">
  <?php // Display blog posts on any page @ https://m0n.co/l
    $temp = $wp_query; $wp_query= null;
    $wp_query = new WP_Query(); $wp_query->query('posts_per_page=3'. '&paged='.$paged);
    while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <a href="<?php esc_url( the_permalink() ); ?>" class="obm-item">
              <span class="obm-img"><img src="<?php echo get_the_post_thumbnail_url($post, 'thumbnail' ); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"></span>
              <h4 class="obm-title"><?php the_title(); ?></h4>
              <span class="obm-links">
                <time class="obm-time"><?php echo get_the_date(); ?></time>
                <span  class="obm-link">Read More</span>
              </span>
            </a>
  <?php endwhile; ?>
    <?php wp_reset_query(); ?>
            
          </div>
        </div>
      </section>
      <!-- Our Blogs Module Ends -->





<?php elseif( get_row_layout() == 'product_hover_module' ): ?>
      <!-- Product hover module -->
      <section class="Product-hover-module">
        <div class="inner-wrap">
          <h2 class="phm-title"><?php if(get_sub_field('phm_title')): echo get_sub_field('phm_title'); endif; ?></h2>

         <?php 
                            $phm_subtitle_link = get_sub_field('phm_subtitle_link');
                            if( $phm_subtitle_link ): 
                                $link_url = $phm_subtitle_link['url'];
                                $link_title = $phm_subtitle_link['title'];
                            ?> <a href="<?php echo esc_url($link_url);?>" class="phm-subtitle"><?php echo esc_html($link_title);?></a> <?php endif; ?>
          <div class="phm-items-wrap">
          	  <?php if( have_rows('phm_items') ):while ( have_rows('phm_items') ) : the_row(); ?>	
           <?php 
                            $phmi_link = get_sub_field('phmi_link');
                            if( $phmi_link ): 
                                $link_url = $phmi_link['url'];
                                $link_title = $phmi_link['title'];
                            ?>  <a class="phm-item" href="<?php echo esc_url($link_url);?>">
              <figure class="phm-img">
               <?php $image = get_sub_field('phmi_image');
                   if( !empty($image) ): ?> <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" title="<?php echo $image['alt'];?>"><?php endif; ?>
              </figure>
              <h3 class="phmc-title up-title"><?php if(get_sub_field('phmi_title')): echo get_sub_field('phmi_title'); endif; ?></h3>
              <div class="phm-content">
                <h3 class="phmc-title"><?php if(get_sub_field('phmi_title')): echo get_sub_field('phmi_title'); endif; ?></h3>
                <p class="phmc-txt"><?php if(get_sub_field('phmi_text')): echo get_sub_field('phmi_text'); endif; ?></p>
                <span class="btn-alt-2 btn-phmc"><?php echo esc_html($link_title);?></span>
              </div>
            </a>
            <?php endif; ?>
             <?php endwhile; endif; ?>
          </div>
        </div>
      </section>
        <!-- Product hover module -->


















      

<?php elseif( get_row_layout() == 'img_gallery_slider' ): ?>
	<?php if( get_sub_field('img_slider')): ?>

		<?php 

		$img = get_sub_field('img_slider');

		if( $img ): ?>
 <!--image gallery -->
        <section class="image-gallery-with-thumbs">
        <div class="inner-wrap">
        <h2><?php if(get_sub_field('ig_head_n')): echo get_sub_field('ig_head_n'); endif; ?></h2>

        <div class="igt-wrap">
        <div class="igwt-left-sec"></div>

        <div class="igwt-innerpage-carousel">
          <div id="sync1" class="owl-carousel owl-theme">
           <?php foreach( $img as $image ): ?>
            <div class="item"><img src="<?php echo $image['sizes']['large']; ?>" alt="<?php $image['alt']; ?>" title="<?php $image['alt']; ?>"/></div>
            <?php endforeach; ?>
          </div>

          <div id="sync2" class="owl-carousel owl-theme">
<?php foreach( $img as $image ): ?>
              <div class="item"><img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php $image['alt']; ?>" title="<?php $image['alt']; ?>"/></div>
              <?php endforeach; ?>
          </div>
            

          </div>
        </div>
        <?php if( get_sub_field('divider')): ?>
					<hr>
				<?php endif; ?>
        </div>
     
       
        </section>
        <!--image gallery -->
        <?php endif; ?>

<?php endif; ?>






<?php elseif( get_row_layout() == 'about_us_module' ): ?>
        <!-- Why Work With Us Module Start-->
            <section class="why-work-module" style="background-image: url(<?php echo get_sub_field('bg_image'); ?>);">
              <div class="inner-wrap">
     <h2 class="aum-heading"><?php if(get_sub_field('section_header')): echo get_sub_field('section_header'); endif;?> <img src="<?php bloginfo('template_url'); ?>/img/logo-element.png" alt="About Us"></h2>
                <div class="wwm-content">
                  <p class="wwm-txt "><?php echo get_sub_field('desc'); ?></p>
     <a class="btn aum-btn" href="<?php echo get_sub_field('cta_link'); ?>"><span><?php echo get_sub_field('cta_title'); ?></span></a>
                </div>
                <div class="wwm-cert">

                  <?php if( have_rows('certification_info') ): while ( have_rows('certification_info') ) : the_row(); ?>
                  <?php if(get_sub_field('certificate_link')): ?>
                  <a href="<?php echo get_sub_field('certificate_link'); ?>" class="wwmc-item">
                    <?php $images = get_sub_field('certificate_image');if( !empty($images) ): ?>
                    <img src="<?php echo $images['url']; ?>" class="wwmc-img" alt="<?php echo $images['alt']; ?>" title="<?php echo $images['alt'];?>"><?php endif; ?>
                  </a>
                  <?php else: ?>
                  <span class="wwmc-item">
                    <?php $images = get_sub_field('certificate_image');if( !empty($images) ): ?>
                    <img src="<?php echo $images['url']; ?>" class="wwmc-img" alt="<?php echo $images['alt']; ?>" title="<?php echo $images['alt'];?>"><?php endif; ?>
                  </span>
                <?php endif; ?>


                  <?php endwhile; ?>
                <?php endif; ?> 
                  
                </div>
              </div>
            </section>
          <!-- Why Work With Us Module End -->


<?php elseif( get_row_layout() == 'manufacturer_logo_module' ): ?>
        <!-- Manufacturer Logo Module Start-->
            <section class="manufacturer-logo-module">
              <div class="inner-wrap">
                <div class="mlm-logo">

                  <?php if( have_rows('logo_item') ): while ( have_rows('logo_item') ) : the_row(); ?>
                  <div class="mlm-item">
                    <?php $images = get_sub_field('logo_image');if( !empty($images) ): ?>
                    <img src="<?php echo $images['url']; ?>" class="wwmc-img" alt="<?php echo $images['alt']; ?>" title="<?php echo $images['alt'];?>"><?php endif; ?>
                    <?php if(get_sub_field('logo_caption')): ?><h6 class="mlm-caption"><?php echo get_sub_field('logo_caption'); ?></h6><?php endif; ?>
                  </div>
                  
                  <?php endwhile; ?>
                <?php endif; ?> 
                  
                </div>
              </div>
            </section>
          <!-- Manufacturer Logo Module End -->







<?php endif; ?>
<?php endwhile; echo '</section>'; ?>
<?php endif; ?>




